<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>HOME</title>

</head>
	<body>
		<h1>Welcome <%=session.getAttribute("fullName")%>! </h1>
		<%
			if(session.getAttribute("userType").equals("applicant")){
				out.println("<p>Welcome "+session.getAttribute("userType")+". You may now start looking for you career opportunity.</p>");
			}else if(session.getAttribute("userType").equals("employer")){
				out.println("<p>Welcome "+session.getAttribute("userType")+". You may now start browsing applicant profiles.</p>");
			}
		%>
		<form action="index.jsp">
					<input type="submit" value="Back">
				</form>
	</body>
</html>